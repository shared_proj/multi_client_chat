import struct


class MSGConstats(object):
    ACKMSG = 1
    NameMSG = 2
    MSG = 3
    REGACK = 4
    MSG_SIZE = 4
    MAX_PACKET = 4096


class Message(object):
    def _recv(self, sock):
        """
            recieve data from socket
        :param sock: socket object
        :return: return string
        """
        data = None
        msg_size = 0
        msg_len = 0
        while msg_size < MSGConstats.MSG_SIZE:
            msg_len = sock.recv(MSGConstats.MSG_SIZE)
            msg_size += len(msg_len)
        if msg_len:
            data = ''
            msg_len = struct.unpack('>I', msg_len)[0]
            data_size = 0
            while data_size < msg_len:
                rest = msg_len - data_size
                chunk = sock.recv(MSGConstats.MAX_PACKET if rest > MSGConstats.MAX_PACKET else rest)
                if not chunk:
                    data = None
                    break
                else:
                    data += chunk
                    data_size += len(chunk)
        return data

    def _send(self, sock, message):
        """
            send stream of bytes to socket
        :param sock: socket object
        :param message: str
        :return:
        """
        sock.send("{0}{1}".format(struct.pack(">II", MSGConstats.MSG, len(message)), message))


class ACKMessage(Message):
    def _send(self, sock, message=""):
        sock.send("{0}{1}".format(struct.pack(">II", MSGConstats.ACKMSG, len(message)), message))


class REGACK(Message):
    def _send(self, sock, message=""):
        sock.send("{0}{1}".format(struct.pack(">II", MSGConstats.REGACK, len(message)), message))


class NameMessgae(Message):
    def _send(self, sock, message):
        sock.send("{0}{1}".format(struct.pack(">II", MSGConstats.NameMSG, len(message)), message))


class Msg(Message):
    pass


class MessageFactory(object):
    def get(self, msg_type):
        if msg_type == MSGConstats.ACKMSG:
            return ACKMessage()
        if msg_type == MSGConstats.NameMSG:
            return NameMessgae()
        if msg_type == MSGConstats.MSG:
            return Msg()
        if msg_type == MSGConstats.REGACK:
            return REGACK()
        return None


def check_message_type(factory, sock):
    """
        check message type
    :param sock: socket object
    :return: Message Object
    """
    msg_obj = None
    total = 0
    while total < 4:
        msg_type = sock.recv(4)
        total += len(msg_type)
        if msg_type:
            # Unpacks the message and gets the message length
            msg_type = struct.unpack('>I', msg_type)[0]
            msg_obj = factory.get(msg_type)
    return msg_obj
