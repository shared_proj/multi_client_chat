#!/bin/python

import sys
import socket
import select
import struct
import datetime
import Messages
import signal


class Server(object):
    def __init__(self, host, port):
        self.servername = host
        self.port = port
        self.socketslist = []
        self.clients = {}
        self.running = True
        self.factory = Messages.MessageFactory()
        signal.signal(signal.SIGINT, self.signal_handler)

    def signal_handler(self, signal, frame):
        """
            signal hadnler
        :param signal: sig number
        :param frame:
        :return:None
        """
        self.stop()
        sys.exit(0)

    def _bind_socket(self):
        """
            connect to server socket
        :return:
        """
        self.serversocket = socket.socket()
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serversocket.bind((self.servername, self.port,))
        self.serversocket.listen(10)
        self.serversocket.setblocking(0)
        self.socketslist.append(self.serversocket)
        print "{0}\nStarting Chat Server...".format(datetime.datetime.now())

    def _run(self):
        """
            run the main server thread
        :return:None
        """
        print "Start listening..."
        while self.running:
            print "{0} online users:{1}".format(datetime.datetime.now(), [n['name'] for n in self.clients.values()])
            try:
                ready_to_read, ready_to_write, in_error = select.select(self.socketslist, [], [], 10)
            except socket.error:
                continue
            else:
                for s in ready_to_read:
                    if s == self.serversocket:
                        try:
                            new_s, addr = s.accept()
                            print "logger: new client added {0}".format(new_s.getpeername())
                            msg_obj = self.factory.get(Messages.MSGConstats.ACKMSG)
                            msg_obj._send(new_s)
                        except socket.error:
                            break
                        self.socketslist.append(new_s)
                        self.clients[new_s] = {"name": "", "reg": False}
                    else:
                        try:
                            msg_obj = self.check_message_type(s)  # Gets the client message...
                            if msg_obj:
                                print "{0} Recieve {1} Message".format(datetime.datetime.now(), type(msg_obj))
                                data = msg_obj._recv(s)
                                if data:
                                    if type(msg_obj) == Messages.NameMessgae:
                                        self.clients[s]['name'] = data
                                        Messages.REGACK()._send(s)
                                        self.clients[s]['reg'] = True
                                        self.sendall(s, Messages.Msg(),
                                                     "\n<SERVER>~~~[{0}] entered the chat room~~~".format(
                                                         self.clients[s]['name']))
                                    else:
                                        self.sendall(s, msg_obj, "\r<{0}>{1}".format(self.clients[s]['name'], data))
                        except socket.error as e:
                            self.sendall(s, Messages.Msg(),
                                         "\r<SERVER>Client ({0}) Left Room.".format(self.clients[s]['name']))
                            print "{0}<SERVER>Client ({1}) Left Room.".format(datetime.datetime.now(),
                                                                              self.clients[s]['name'])
                            s.close()
                            self.socketslist.remove(s)
                            del self.clients[s]
                            continue
        self.stop()

    def start_server(self):
        """
            Given a host and a port, binds the socket and runs the server.
        :return:
        """
        self._bind_socket()
        self._run()

    def check_message_type(self, sock):
        """
            check message type
        :param sock: socket object
        :return: Message Object
        """
        msg_obj = None
        tot_len = 0
        while tot_len < Messages.MSGConstats.MSG_SIZE:
            msg_type = sock.recv(Messages.MSGConstats.MSG_SIZE)
            tot_len += len(msg_type)
            if msg_type != "":
                msg_type = struct.unpack('>I', msg_type)[0]
                msg_obj = self.factory.get(int(msg_type))
            else:
                raise socket.error
        return msg_obj

    def sendall(self, client_sock, msg_obj, msg):
        """
            send message to every client
        :param client_sock:
        :param msg_obj:
        :param msg:
        :return:
        """
        for sock in self.socketslist:
            if sock != client_sock and sock != self.serversocket:
                try:
                    if msg and self.clients[sock]['reg']:
                        msg_obj._send(sock, msg)
                except Exception as e:
                    print e
                    sock.close()
                    self.socketslist.remove(sock)
                    del self.clients[sock]

    def stop(self):
        """
            stop all services
        :return:
        """
        self.serversocket.close()
        self.running = False


if __name__ == '__main__':
    if len(sys.argv) == 3:
        server = Server(sys.argv[1], int(sys.argv[2]))
        server.start_server()
    else:
        print "Pleas re-run [usage]: <server_adress> <port>"
        exit(1)
