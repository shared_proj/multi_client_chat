#!/usr/bin/python

import sys
import os
import time
import socket
import select
import threading
import Queue
import random
import Messages
import termios
import tty
import signal

TIMEOUT = 5  # number of seconds your want for timeout
DELAY = 2
MAX_TYPE_SIZE = 4


class Client(object):
    def __init__(self, host, port):
        self.servername = host
        self.port = port
        self.r_socketslist = []
        self.name_flag = False
        self.running = True
        self.msg_sender = Messages.Msg()
        self.factory = Messages.MessageFactory()
        self.msg_queue = Queue.Queue()
        self.q_flag = False
        self.old_settings = None
        signal.signal(signal.SIGINT, self.signal_handler)

    def signal_handler(self, signal, frame):
        """
            signal handler
        :param signal: signal number
        :param frame:
        :return: None
        """
        if self.name_flag:
            self.stop()

    def _bind_socket(self):
        """
            connect to server socket
        :return:
        """
        self.serversocket = socket.socket()
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.r_socketslist.append(self.serversocket)
        try:
            self.serversocket.connect((self.servername, self.port))
        except socket.error as e:
            print e
            exit(1)

    def print_introduction(self):
        """
            print introduction
        :return:
        """
        print "\n\n"
        print"\t***************************************************************"
        print"\t*                                                             *"
        print"\t*              ~~~~~Welcome to T-Chat~~~~~                    *"
        print"\t*                                                             *"
        print"\t*          1.Enter Name.                                      *"
        print"\t*          2.Press any key to type message                    *"
        print"\t*          3.Press ESC to Exit from type_message mode         *"
        print"\t*          4.After 10 Seconds without exit type_message mode  *"
        print"\t*          5.Exit - Press CTRL-C                              *"
        print"\t*                ****Matan Ramrazker****                      *"
        print"\t***************************************************************"
        print "\n\n"

    def _run(self):
        """
            main thread that listen to socket server and add actions
        :return:
        """
        while self.running:
            try:
                rs, ws, err = select.select(self.r_socketslist, [], [])
            except select.error as ex:
                print ex
                continue
            for s in rs:
                try:
                    # msg_obj = self.check_message_type(s)
                    msg_obj = Messages.check_message_type(self.factory, s)

                    if msg_obj:
                        if not self.name_flag:
                            if msg_obj and type(msg_obj) == Messages.REGACK:
                                self.name_flag = True
                        else:
                            message = msg_obj._recv(s)
                            if message:
                                self.msg_queue.put(message)
                except socket.error as e:
                    self.stop()
        self.stop()

    def tracekeyboard(self):
        """
            listen and trace to stdin - and simulate cmd promt
        :return: None
        """

        def is_data():
            """
                return True if stdin not empty
            :return: bool
            """
            return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])

        def clean_line(chars):
            sys.stdout.write("\r{0}\r".format(" " * chars))
            sys.stdout.flush()

        idle = time.time()
        self.old_settings = termios.tcgetattr(sys.stdin)
        try:
            tty.setcbreak(sys.stdin.fileno())
            message = ""
            while self.running:
                if is_data():
                    self.q_flag = True
                    idle = time.time()
                    c = sys.stdin.read(1)
                    if not message and c != '\x7f':
                        message = message[:-1]
                        sys.stdout.write("\rMESSAGE>")
                        sys.stdout.flush()
                    if c == '\x7f' and message:
                        message = message[:-1]
                        if message:
                            message = message[:-1]
                            sys.stdout.write("\b \b\rMESSAGE>{0} \b".format(message))
                            sys.stdout.flush()
                        continue
                    message += c
                    if c == '\n':
                        try:
                            if message:
                                if self.name_flag:
                                    message = message[:-1]
                                    if message:
                                        self.msg_sender._send(self.serversocket, "{0}".format(message))
                                        self.msg_queue.put("\r{0}>{1}".format('[ME]', message))
                                        clean_line(len("MESSAGE>") + len(message))
                                    message = ""
                                    self.q_flag = False
                                    continue
                        except socket.error as e:
                            print e
                            self.serversocket.close()
                            self.stop()
                    if c == '\x1b':  # x1b is ESC
                        clean_line(len("MESSAGE>") + len(message))
                        message = ""
                        self.q_flag = False
                        continue
                    sys.stdout.write(c)
                    sys.stdout.flush()
                if time.time() - idle > 10:
                    clean_line(len("MESSAGE>") + len(message))
                    self.q_flag = False
                    message = ""
        finally:
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.old_settings)
            self.q_flag = False
            os._exit(0)

    def printdata(self):
        """
            print data from message queue - run as thread
        :return:
        """
        while self.running:
            try:
                while self.q_flag or self.msg_queue.empty():
                    time.sleep(DELAY)
                else:
                    print "{0}".format(self.msg_queue.get())
            except KeyboardInterrupt:
                self.stop()

    def start_chat(self):
        """
            start all actors of the server - keyboardtrace,
        :return:
        """
        self._bind_socket()
        self.print_introduction()
        self.register()
        keytrace_tread = threading.Thread(target=self.tracekeyboard)
        p = threading.Thread(target=self.printdata)
        p.start()
        keytrace_tread.start()
        # t = threading.Thread(target=self.inputdata)
        # t.start()
        self._run()
        p.join()
        keytrace_tread.join()

    def stop(self):
        """
            stop all services
        :return:
        """
        if self.old_settings:
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.old_settings)

        self.running = False
        self.q_flag = False
        self.serversocket.close()
        sys.exit(0)

    def register(self):
        """
            register to server ,Tolerance -  3 retries
        :return:
        """
        retries = 0
        while True:
            name = raw_input("please enter your nickname:")
            if name:
                break
            retries += 1
            if retries > 3:
                sys.stderr.write("register did'nt complete.")
                exit(1)

        msg_obj = Messages.NameMessgae()
        try:
            if name:
                msg_obj._send(self.serversocket, name)
        except socket.error as e:
            print e
            self.stop()

    def inputdata(self):
        """
            simulate human behavior working in test mode nees to add thread to
        :return:
        """
        messages = ['hi', 'hello', '????', 'how r u sir?', 'xtremio wowww!!!']
        while self.running:
            time.sleep(random.randint(0, 5))
            m = messages[random.randint(0, len(messages) - 1)]
            message = "\r{0}>{1}".format('[ME]', m)
            self.msg_queue.put(message)
            try:
                self.msg_sender._send(self.serversocket, m)
            except socket.error:
                self.stop()
                exit(1)


if __name__ == '__main__':
    if len(sys.argv) == 3:
        cl = Client(sys.argv[1], int(sys.argv[2]))
        cl.start_chat()
    else:
        print "Please re-run [usage]: <server_address> <port>"
        exit(1)
